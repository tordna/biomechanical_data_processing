#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail
if [[ "${TRACE-0}" == "1" ]]; then
    set -o xtrace
fi

if [ "$#" -ne 1 ] || ! [ -d "$1" ]; then
    echo 'Usage: ./script.sh /absolute/path/to/install/dir

This is a OpenSim installer for Ubuntu 22 LTS.

'
    exit
fi


OPENSIM_INSTALL_DIR=$(realpath $1)
cd "$OPENSIM_INSTALL_DIR"

main() {

    # Install dependencies from package manager.
    sudo apt-get update && sudo apt-get install --yes build-essential cmake cmake-curses-gui coinor-libipopt-dev autotools-dev autoconf pkg-config automake libopenblas-dev liblapack-dev freeglut3-dev libxi-dev libxmu-dev doxygen python3 python3-dev python3-numpy python3-setuptools openjdk-8-jdk libpcre3 libpcre3-dev byacc git gfortran libtool


    # Download and install SWIG.
    mkdir -p $OPENSIM_INSTALL_DIR/opensim-workspace/swig-source && cd $OPENSIM_INSTALL_DIR/opensim-workspace/swig-source
    wget https://github.com/swig/swig/archive/refs/tags/rel-4.0.2.tar.gz
    tar xzf rel-4.0.2.tar.gz && cd swig-rel-4.0.2
    sh autogen.sh && ./configure --prefix=$OPENSIM_INSTALL_DIR/swig --disable-ccache
    make && make -j4 install  

    # Download and install NetBeans 12.3.
    mkdir -p $OPENSIM_INSTALL_DIR/opensim-workspace/Netbeans12.3 && cd $OPENSIM_INSTALL_DIR/opensim-workspace/Netbeans12.3
    wget -q https://archive.apache.org/dist/netbeans/netbeans/12.3/Apache-NetBeans-12.3-bin-linux-x64.sh
    chmod 755 Apache-NetBeans-12.3-bin-linux-x64.sh
    ./Apache-NetBeans-12.3-bin-linux-x64.sh --silent 
    mv $HOME/netbeans-12.3 . #TODO: Fix install dir for NetBeans

    # Get opensim-core.
    git clone https://github.com/opensim-org/opensim-core.git $OPENSIM_INSTALL_DIR/opensim-workspace/opensim-core-source

    # Build opensim-core dependencies.
    mkdir -p $OPENSIM_INSTALL_DIR/opensim-workspace/opensim-core-source/dependencies/build
    cd $OPENSIM_INSTALL_DIR/opensim-workspace/opensim-core-source/dependencies/build
    cmake $OPENSIM_INSTALL_DIR/opensim-workspace/opensim-core-source/dependencies -DCMAKE_INSTALL_PREFIX=$OPENSIM_INSTALL_DIR'/opensim-workspace/opensim-core-dependencies/' -DCMAKE_BUILD_TYPE='Release' -DSUPERBUILD_simbody=ON -DSUPERBUILD_spdlog=ON -DSUPERBUILD_ezc3d=ON -DSUPERBUILD_docopt=ON -DSUPERBUILD_BTK=OFF -DOPENSIM_WITH_CASADI=ON -DOPENSIM_WITH_TROPTER=ON
    make -j8

    # Build opensim-core.
    mkdir -p $OPENSIM_INSTALL_DIR/opensim-workspace/opensim-core-source/build
    cd $OPENSIM_INSTALL_DIR/opensim-workspace/opensim-core-source/build
    cmake ../ -DCMAKE_INSTALL_PREFIX="$OPENSIM_INSTALL_DIR/opensim-core" -DCMAKE_BUILD_TYPE='RelWithDebInfo' -DOPENSIM_DEPENDENCIES_DIR="$OPENSIM_INSTALL_DIR/opensim-workspace/opensim-core-dependencies/" -DOPENSIM_C3D_PARSER=ezc3d -DBUILD_PYTHON_WRAPPING=ON -DBUILD_JAVA_WRAPPING=ON -DWITH_BTK=OFF -DWITH_EZC3D=ON -DBUILD_TESTING=OFF -DOPENSIM_INSTALL_UNIX_FHS=OFF -DOPENSIM_COPY_DEPENDENCIES=ON -DSWIG_DIR=$OPENSIM_INSTALL_DIR/swig/share/swig -DSWIG_EXECUTABLE=$OPENSIM_INSTALL_DIR/swig/bin/swig/ -DOPENSIM_COPY_DEPENDENCIES=ON -DCMAKE_BUILD_TYPE='Release'  -DOPENSIM_WITH_CASADI=ON -DOPENSIM_WITH_TROPTER=ON
    make -j2
    make install -j2

    # Get opensim-gui.
    git clone https://github.com/opensim-org/opensim-gui.git $OPENSIM_INSTALL_DIR/opensim-workspace/opensim-gui-source
    cd $OPENSIM_INSTALL_DIR/opensim-workspace/opensim-gui-source
    git submodule update --init --recursive -- opensim-models opensim-visualizer Gui/opensim/threejs

    # Build opensim-gui.
    mkdir -p $OPENSIM_INSTALL_DIR/opensim-workspace/opensim-gui-source/build
    cd $OPENSIM_INSTALL_DIR/opensim-workspace/opensim-gui-source/build
    cmake ../ -DCMAKE_PREFIX_PATH="$OPENSIM_INSTALL_DIR/opensim-core" -DAnt_EXECUTABLE="$OPENSIM_INSTALL_DIR/opensim-workspace/Netbeans12.3/netbeans-12.3/netbeans/extide/ant/bin/ant" -DANT_ARGS="-Dnbplatform.default.netbeans.dest.dir=$OPENSIM_INSTALL_DIR/opensim-workspace/Netbeans12.3/netbeans-12.3/netbeans;-Dnbplatform.default.harness.dir=$OPENSIM_INSTALL_DIR/opensim-workspace/Netbeans12.3/netbeans-12.3/netbeans/harness"
    make CopyOpenSimCore
    make PrepareInstaller

    # Install opensim-gui.
    cd $OPENSIM_INSTALL_DIR/opensim-workspace/opensim-gui-source/Gui/opensim/dist/installer/opensim
    bash INSTALL
}

main "$@"